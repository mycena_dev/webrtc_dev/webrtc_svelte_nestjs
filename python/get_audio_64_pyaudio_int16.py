from queue import Queue
from threading import Thread
import numpy as np
import redis
import sys
import base64
# import sounddevice as sd
import pyaudio


redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)
topicKey = "test_video"

if sys.argv[1]:
    topicKey = sys.argv[1]

# frq = 1/25
channels=2
catch_time = 0
get_count = 0
max_count = 100
latency_count = []
keepConsuming = True
keepPlaying = True
audio_store = Queue()

def play_audio():
    global audio_store
    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paInt16,
                channels=2,
                rate=44100,
                output=True)
    while keepPlaying:
        try:
            audio_data = audio_store.get(timeout=0.5)
            stream.write(audio_data.tobytes())
            # sd.play(audio_data, samplerate=44100)
            # sd.wait()

        except Exception as e:
            print("e:" , e)
            print("Audio Empty")
    stream.stop_stream()
    stream.close()
    p.terminate()

if __name__ == "__main__":
    keepPlaying = True
    keepConsuming = True
    old_chunk = ""
    play_thread = Thread(target=play_audio, daemon=True)
    play_thread.start()
    try:
        while keepConsuming:
            base64_str = redis_client.get(topicKey)
            if base64_str == old_chunk:
                continue
            old_chunk = base64_str
            audio_bytes = base64.b64decode(base64_str)

            audio_data = np.frombuffer(audio_bytes, np.int16)
            audio_data = audio_data.reshape(-1, channels)
            audio_store.put(audio_data)

    except KeyboardInterrupt:
        keepConsuming = False
        keepPlaying = False
        print("WARN: Keyboard Interrupt detected. Exiting...")


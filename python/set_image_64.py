# sudo docker run --rm --name some-redis -p 6379:6379 redis

import redis
import time
import cv2
import sys
import base64

video_dic = {
                "v1": "無神世界中的神明活動 OP-I wish.mp4",
                "v2": "SampleVideo_1280x720_5mb.mp4",
                "v3": "XG - LEFT RIGHT (XG SHOOTING STAR LIVE STAGE).mp4",
                "v4": "XG - MASCARA (Performance Video).mp4",
                "v6":"DEMON SLAYER_ KIMETSU NO YAIBA OP 4.mp4",
                "v5": "YOASOBI - IDOL (OSHI NO KO OP).mp4",
            }

allow_topic_list = ["v1", "v2", "v3", "v4", "v5"]
redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)
videoFile = "SampleVideo_1280x720_5mb.mp4"
frp = 1/25

if len(sys.argv) < 2:
    raise "Need to provide topic name."

if sys.argv[1]:
    topicKey = sys.argv[1]
    videoFile = video_dic.get(topicKey)
    
print(topicKey)

while 1:
    __VIDEO_FILE = cv2.VideoCapture(videoFile)
    try: 
        while(__VIDEO_FILE.isOpened()):
            readStat, frame = __VIDEO_FILE.read()
            if not readStat:
                __VIDEO_FILE.release()
                break
            frame = cv2.resize(frame, (1280, 720))
            _, buffer = cv2.imencode('.jpg', frame)
            redis_client.set(topicKey, base64.b64encode(buffer))
            time.sleep(frp)

    except KeyboardInterrupt:
        __VIDEO_FILE.release()
        print("Keyboard interrupt was detected. Exiting...")
        break    
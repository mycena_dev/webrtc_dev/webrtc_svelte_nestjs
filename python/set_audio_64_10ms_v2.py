# sudo docker run --rm --name some-redis -p 6379:6379 redis

import redis
import time
import cv2
import sys
import base64
import moviepy.editor
import numpy as np

video_dic = {
                "v1": "無神世界中的神明活動 OP-I wish.mp4",
                "v2": "SampleVideo_1280x720_5mb.mp4",
                "v3": "XG - LEFT RIGHT (XG SHOOTING STAR LIVE STAGE).mp4",
                "v4": "XG - MASCARA (Performance Video).mp4",
                "v6":"DEMON SLAYER_ KIMETSU NO YAIBA OP 4.mp4",
                "v5": "YOASOBI - IDOL (OSHI NO KO OP).mp4",
            }

allow_topic_list = ["v1", "v2", "v3", "v4", "v5"]
redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)
videoFile = "SampleVideo_1280x720_5mb.mp4"
frp = 1/25

if len(sys.argv) < 2:
    raise "Need to provide topic name."

if sys.argv[1]:
    topicKey = sys.argv[1]
    videoFile = video_dic.get(topicKey)
    
print(topicKey)

video = moviepy.editor.VideoFileClip(videoFile)

duration = video.duration
duration_ms = int(duration * 1000)
print(duration)
# total_frames = video.reader.nframes * 8
# frame_rate = video.fps*8
# print(total_frames)
# print(frame_rate)
# exit(0)
step_time = 10 # ms
while 1:
    try: 
        for frame_time in range(0, duration_ms,  step_time):
            # print(frame_time)
            # print("====")
            if frame_time + step_time >= duration_ms:
                break  # Exit the loop if frame_time exceeds video duration

            audio_chunk = video.audio.subclip(frame_time/1000, (frame_time + step_time)/1000).to_soundarray()
            if len(audio_chunk) > 0:
                scaled_arr = np.clip(audio_chunk * 32767, -32768, 32767)
                
                # 將浮點數轉換為整數
                scaled_chunk = scaled_arr.astype(np.int16)
                if scaled_chunk.shape[0] != 441:
                    scaled_chunk = scaled_chunk[:441, :]

                # 对字节流进行Base64编码
                base64_str = base64.b64encode(scaled_chunk)
                # print(base64_str)
                redis_client.set(topicKey, base64_str)       
                time.sleep(step_time/1000)

    except KeyboardInterrupt:
        print("Keyboard interrupt was detected. Exiting...")
        break    
    except Exception as e:
        print(e);
        continue
from queue import Queue
from threading import Thread
import numpy as np
import redis
import time
import cv2
import sys
import base64

redis_client = redis.StrictRedis(host='localhost', port=6379, db=0)
topicKey = "test_video"

if sys.argv[1]:
    topicKey = sys.argv[1]

# frq = 1/25

catch_time = 0
get_count = 0
max_count = 100
latency_count = []
keepConsuming = True
keepPlaying = True
currentFrame = cv2.imencode('.jpg',np.zeros(shape=(1080,1920,3), dtype=np.uint8))[1].tobytes()

if __name__ == "__main__":
    keepPlaying = True
    keepConsuming = True
    cv2.namedWindow("frame", cv2.WINDOW_NORMAL)
    try:
        while keepConsuming:
            value = redis_client.get(topicKey)
            with open('hex.txt', 'w') as f:
                f.write(str(value))
            # print(str(value[2:-1]))
            nparr = np.fromstring(base64.b64decode(value), np.uint8)
            frame = cv2.imdecode(nparr, cv2.IMREAD_ANYCOLOR)
            cv2.imshow('frame', frame)
        
            if cv2.waitKey(1) & 0xFF == ord('q'):
                keepConsuming = False
                break

    except KeyboardInterrupt:
        keepConsuming = False
        keepPlaying = False
        print("WARN: Keyboard Interrupt detected. Exiting...")


import { Test, TestingModule } from '@nestjs/testing';
import { PeerController } from './peer.controller';
import { PeerService } from './peer.service';

describe('PeerController', () => {
  let controller: PeerController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PeerController],
      providers: [PeerService],
    }).compile();

    controller = module.get<PeerController>(PeerController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

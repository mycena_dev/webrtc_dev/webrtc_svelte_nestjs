import {
  Controller,
  Post,
  Body,
} from '@nestjs/common';
import * as webrtc from 'wrtc';
import { createClient } from '@redis/client';
import * as sdpTransform from 'sdp-transform';


@Controller('audio-subscribe')
export class AudioSubscribeController {
  
  private client = createClient({
    url: 'redis://localhost:6379'
  });

  private isConnect = false;

  constructor() {
      this.client.connect().then(() => {
        this.isConnect = true;
      });
    }

  @Post('')
  async audioCon(@Body() params) {
    const offer = params;
    const pc = new webrtc.RTCPeerConnection();

    const easySDP = sdpTransform.parse(offer.sdp);
    // console.log(easySDP)

    const audioNum = easySDP.media.filter(media => media.type === 'audio').length;
    // console.log(audioNum)

    const trackIntervalList = []

    const mediaStream = new webrtc.MediaStream();

    for (let i = 0; i < audioNum; i++) {
      const source = new webrtc.nonstandard.RTCAudioSource();  
      const track = source.createTrack();
      console.log("!!!!!!!!!!!!!!!! Create Audio Source !!!!!!!!!!!!!!")
      const interval = setInterval(async () => {

        const sampleRate = 44100;
        // const channel = 1;
        const samples = new Int16Array(44100 / 100 * 2);
        
        const chunk = {
          samples,
          sampleRate,
          channelCount: 2,
          bitsPerSample: 16
        };

        if(this.isConnect){
          const topic = `v${i+1}`
          const audioStr = await this.client.get(topic);

          const base64Audio = Buffer.from(audioStr, 'base64');
          // const newSamples_wrong = new Int16Array(base64Audio);
          // console.log(newSamples_wrong)

          const newSamples = new Int16Array(base64Audio.buffer, base64Audio.byteOffset, base64Audio.length / Int16Array.BYTES_PER_ELEMENT);
          
          // console.log("BYTES_PER_ELEMENT", newSamples.BYTES_PER_ELEMENT, samples.BYTES_PER_ELEMENT)
          // console.log("buffer", newSamples.buffer, samples.buffer)
          // console.log("byteLength", newSamples.byteLength, samples.byteLength)
          // console.log("byteOffset", newSamples.byteOffset, samples.byteOffset)

          samples.set(newSamples);
        }
        try{
          source.onData(chunk);
        }catch(e){
          console.log(e);
        }
      }, 10)
      trackIntervalList.push(interval);
      mediaStream.addTrack(track);
      pc.addTrack(track, mediaStream);
    }
        
    pc.ondatachannel = (event) => { 
      event.channel.onmessage = (event) => {
        console.log(event.data);
          if(event.data === "close"){
          console.log(`Client ${offer.sdp.match("c=IN.*.")[0].split(" ").slice(-1)[0]} disconnect.`);
          pc.close();
        }
      }
    }

    pc.onconnectionstatechange = () => {
      switch (pc.connectionState) {
        case "new":
          case "checking":
            console.log("Connecting…");
            break;
          case "connected":
            console.log("Online");
            break;
          case "disconnected":
            console.log("Disconnecting…");
            pc.close();
            for(const interval of trackIntervalList){
              clearInterval(interval)
            }
            break;
          case "closed":
            console.log("Offline");
            pc.close();
            for(const interval of trackIntervalList){
              clearInterval(interval)
            }            break;
          case "failed":
            console.log("Error");
            break;
          default:
            console.log("Unknown");
            break;
      }
    }

    pc.ontrack = (event) => {
    }

    await pc.setRemoteDescription(offer);
    const answer = await pc.createAnswer();
    await pc.setLocalDescription(answer);
    return {"sdp": pc.localDescription.sdp, "type": "answer"};
  }

}

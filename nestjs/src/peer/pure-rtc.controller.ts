import {
  Controller,
  Post,
  Body,
} from '@nestjs/common';
import * as webrtc from 'wrtc';

@Controller('pure-offer')
export class PureRTCController {

  sentence_list = ["Hello!", "Any question?", "This is a good day!", "ByeBye <3"]


  constructor() {
    }

  @Post('')
  async videoCon(@Body() params) {
    console.log(params)
    const offer = params;
    const pc = new webrtc.RTCPeerConnection();

    pc.ondatachannel = (channelEvent) => { 
      channelEvent.channel.onmessage = (event) => {
        console.log(event.data);
          if(event.data === "close"){
          console.log(`Client ${offer.sdp.match("c=IN.*.")[0].split(" ").slice(-1)[0]} disconnect.`);
          pc.close();
        }else{
          channelEvent.channel.send(this.sentence_list[Math.floor(Math.random()*this.sentence_list.length)]);
        }
      }
    }

    pc.onconnectionstatechange = () => {
      switch (pc.connectionState) {
        case "new":
          case "checking":
            console.log("Connecting…");
            break;
          case "connected":
            console.log("Online");
            break;
          case "disconnected":
            console.log("Disconnecting…");
            pc.close();
            break;
          case "closed":
            console.log("Offline");
            pc.close();
            break;
          case "failed":
            console.log("Error");
            break;
          default:
            console.log("Unknown");
            break;
      }
    }

    await pc.setRemoteDescription(offer);
    const answer = await pc.createAnswer();
    await pc.setLocalDescription(answer);
    return {"sdp": pc.localDescription.sdp, "type": "answer"};
  }

}

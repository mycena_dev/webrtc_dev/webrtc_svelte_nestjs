import { Module } from '@nestjs/common';
import { PeerController } from './peer.controller';
import { PureRTCController } from './pure-rtc.controller';
import { Video2WayController } from './video-2-way.controller';
import { VideoSubscribeController } from './video-subscriber.controller';
import { Audio2WayController } from './audio-2-way.controller';
import { AudioSubscribeController } from './audio-subscriber.controller';

@Module({
  controllers: [PeerController, PureRTCController, Video2WayController, VideoSubscribeController, Audio2WayController, AudioSubscribeController],
  providers: []
})
export class PeerModule {}

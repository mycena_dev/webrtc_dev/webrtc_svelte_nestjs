import {
  Controller,
  Post,
  Body,
} from '@nestjs/common';
import * as webrtc from 'wrtc';

@Controller('video-2-way')
export class Video2WayController {
  
  constructor() {
    }

  @Post('')
  async videoCon(@Body() params) {
    const offer = params;
    const pc = new webrtc.RTCPeerConnection();

    pc.ondatachannel = (event) => { 
      event.channel.onmessage = (event) => {
        console.log(event.data);
          if(event.data === "close"){
          console.log(`Client ${offer.sdp.match("c=IN.*.")[0].split(" ").slice(-1)[0]} disconnect.`);
          pc.close();
        }
      }
    }

    pc.onconnectionstatechange = () => {
      switch (pc.connectionState) {
        case "new":
          case "checking":
            console.log("Connecting…");
            break;
          case "connected":
            console.log("Online");
            break;
          case "disconnected":
            console.log("Disconnecting…");
            pc.close();
            break;
          case "closed":
            console.log("Offline");
            pc.close();
            break;
          case "failed":
            console.log("Error");
            break;
          default:
            console.log("Unknown");
            break;
      }
    }


    pc.ontrack = (event) => {
      pc.addTrack(event.track, event.streams[0]);
      
    }

    await pc.setRemoteDescription(offer);
    const answer = await pc.createAnswer();
    await pc.setLocalDescription(answer);
    return {"sdp": pc.localDescription.sdp, "type": "answer"};
  }

}

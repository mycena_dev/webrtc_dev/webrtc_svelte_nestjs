import {
  Controller,
  Post,
  Body,
} from '@nestjs/common';
import * as webrtc from 'wrtc';
import { createClient } from '@redis/client';
import * as jpeg from 'jpeg-js';


@Controller('peer')
export class PeerController {
  
  private client = createClient({
    url: 'redis://localhost:6379'
  });

  private isConnect = false;

  constructor() {
      this.client.connect().then(() => {
        this.isConnect = true;
        // this.client.get('webrtc').then((value) => {

        //   const image = Buffer.from(value, 'base64');

        //   const rawImageData = jpeg.decode(image);
        //   const i420Frame = {
        //     width: rawImageData.width,
        //     height: rawImageData.height,
        //     data: new Uint8ClampedArray(1.5 * rawImageData.width * rawImageData.height)
        //   };
        //   webrtc.nonstandard.rgbaToI420(rawImageData, i420Frame);
        //   console.log(i420Frame)
        // })

      });
    }

  @Post('video/video-2-way')
  async videoCon(@Body() params) {
    const offer = params;
    const pc = new webrtc.RTCPeerConnection();

    pc.ondatachannel = (event) => { 
      event.channel.onmessage = (event) => {
        console.log(event.data);
          if(event.data === "close"){
          console.log(`Client ${offer.sdp.match("c=IN.*.")[0].split(" ").slice(-1)[0]} disconnect.`);
          pc.close();
        }
      }
    }

    const mediaStream = new webrtc.MediaStream();
    const source = new webrtc.nonstandard.RTCVideoSource();
    const track = source.createTrack();
    const sink = new webrtc.nonstandard.RTCVideoSink(track);
    
    const interval = setInterval(async () => {
      // Update the frame in some way before sending.
      const width = 1280;
      const height = 720;
      const data = new Uint8Array(width * height * 1.5);
      const frame = { width, height, data };

      if(this.isConnect){
        const base64Image = Buffer.from(await this.client.get('webrtc'), 'base64')
          const rawImageData = jpeg.decode(base64Image);
          const i420Frame = {
            width: rawImageData.width,
            height: rawImageData.height,
            data: new Uint8Array(1.5 * rawImageData.width * rawImageData.height)
          };
          webrtc.nonstandard.rgbaToI420(rawImageData, i420Frame);
          frame.data = i420Frame.data;
      }
      
      source.onFrame(frame);
    });

    sink.onframe = ({ frame }) => {
      // Do something with the received frame.
    };

    pc.onconnectionstatechange = () => {
      switch (pc.connectionState) {
        case "new":
          case "checking":
            console.log("Connecting…");
            break;
          case "connected":
            console.log("Online");
            break;
          case "disconnected":
            console.log("Disconnecting…");
            clearInterval(interval);
            pc.close();
            break;
          case "closed":
            console.log("Offline");
            clearInterval(interval);
            pc.close();
            break;
          case "failed":
            console.log("Error");
            clearInterval(interval);
            pc.close();
            break;
          default:
            console.log("Unknown");
            break;
      }
    }

    mediaStream.addTrack(track);

    pc.ontrack = (event) => {
      pc.addTrack(track, mediaStream);
    }

    await pc.setRemoteDescription(offer);
    const answer = await pc.createAnswer();
    await pc.setLocalDescription(answer);
    return {"sdp": pc.localDescription.sdp, "type": "answer"};
  }

}

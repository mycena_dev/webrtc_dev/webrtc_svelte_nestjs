import {
  Controller,
  Post,
  Body,
} from '@nestjs/common';
import * as webrtc from 'wrtc';
import { createClient } from '@redis/client';
import * as jpeg from 'jpeg-js';
import * as sdpTransform from 'sdp-transform';


@Controller('video-subscribe')
export class VideoSubscribeController {
  
  private client = createClient({
    url: 'redis://localhost:6379',
  });

  private isConnect = false;

  constructor() {
      this.client.connect().then(() => {
        this.isConnect = true;
      });
    }

  @Post('')
  async videoCon(@Body() params) {
    const offer = params;
    const pc = new webrtc.RTCPeerConnection();

    const easySDP = sdpTransform.parse(offer.sdp);
    console.log(easySDP)

    const videoNum = easySDP.media.filter(media => media.type === 'video').length;
    console.log(videoNum)
    const trackIntervalList = []
    const mediaStream = new webrtc.MediaStream();
    for (let i = 0; i < videoNum; i++) {
      const source = new webrtc.nonstandard.RTCVideoSource();  
      const track = source.createTrack();
      const interval = setInterval(async () => {
        // Update the frame in some way before sending.
        const width = 1280;
        const height = 720;
        const data = new Uint8Array(width * height * 1.5);
        const frame = { width, height, data };
        
        if(this.isConnect){
          const topic = `v${i+1}`
          const base64Image = Buffer.from(await this.client.get(topic), 'base64')
          const rawImageData = jpeg.decode(base64Image);
          const i420Frame = {
            width: rawImageData.width,
            height: rawImageData.height,
            data: new Uint8Array(1.5 * rawImageData.width * rawImageData.height)
          };
          webrtc.nonstandard.rgbaToI420(rawImageData, i420Frame);
          frame.data = i420Frame.data;
        }
        source.onFrame(frame);
      }, (1/25)*1000)
      trackIntervalList.push(interval)
      mediaStream.addTrack(track);
      pc.addTrack(track, mediaStream);
    }
        
    pc.ondatachannel = (event) => { 
      event.channel.onmessage = (event) => {
        console.log(event.data);
          if(event.data === "close"){
          console.log(`Client ${offer.sdp.match("c=IN.*.")[0].split(" ").slice(-1)[0]} disconnect.`);
          pc.close();
        }
      }
    }

    pc.onconnectionstatechange = () => {
      switch (pc.connectionState) {
        case "new":
          case "checking":
            console.log("Connecting…");
            break;
          case "connected":
            console.log("Online");
            break;
          case "disconnected":
            console.log("Disconnecting…");
            pc.close();
            for(const interval of trackIntervalList){
              clearInterval(interval)
            }
            break;
          case "closed":
            console.log("Offline");
            pc.close();
            for(const interval of trackIntervalList){
              clearInterval(interval)
            }            break;
          case "failed":
            console.log("Error");
            break;
          default:
            console.log("Unknown");
            break;
      }
    }

    pc.ontrack = (event) => {
      // console.log("OnTrack!!!!!!!!!!!!!!!!!!, ", event) 
      // pc.addTrack(track, mediaStream);
    }

    await pc.setRemoteDescription(offer);
    const answer = await pc.createAnswer();
    await pc.setLocalDescription(answer);
    return {"sdp": pc.localDescription.sdp, "type": "answer"};
  }

}

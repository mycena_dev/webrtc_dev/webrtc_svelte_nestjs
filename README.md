# WebRTC NestJS 中心架構
![WebRTC Center](webrtc_nest.png)

# Video & Audio 結構圖
![WebRTC Center](video_audio.png)


# Install python requirement

```
cd python

pip install -r requirements.txt
```

# Run Redis

```
sudo docker run --rm --name some-redis -p 6379:6379 redis
```
---
# Video 與 Audio 擇一跑

# Video: Push mp4 to Redis. (依需求跑數量即可)

```
python set_image_64.py v1

python set_image_64.py v2

python set_image_64.py v3

python set_image_64.py v4

python set_image_64.py v5

python set_image_64.py v6
```

# Audio: Push mp4 to Redis. (依需求跑數量即可)

```
python set_audio_64_10ms_v2.py v1

python set_audio_64_10ms_v2.py v2

python set_audio_64_10ms_v2.py v3

python set_audio_64_10ms_v2.py v4

python set_audio_64_10ms_v2.py v5

python set_audio_64_10ms_v2.py v6
```

# Run NestJs

```
cd nestjs

npm i

npm run start:dev
```

# Run Sveltekit

```
cd sveltekit

npm i

npm run dev -- --open

```
